package com.sportradar

import com.sportradar.data.MatchStore
import com.sportradar.mixin.TestData
import com.sportradar.model.Match
import spock.lang.Specification

import java.time.LocalDateTime

class ScoreBoardTest extends Specification implements TestData {

    private ScoreBoard scoreBoard
    private MatchStore matchStore

    def setup() {
        matchStore = Mock(MatchStore)
        scoreBoard = ScoreBoard.createWithStorage(matchStore)
    }

    def "check calling appropriate methods for starting match"() {

        given:
        def arguments = [] as Match[]

        when:
        def returnedMatch = scoreBoard.startMatch(MEXICO, BRAZIL)

        then:
        1 * matchStore.addMatch(_) >> {
            arguments = it
        }
        arguments[0] instanceof Match
        arguments[0].homeTeam() == MEXICO
        arguments[0].awayTeam() == BRAZIL
        arguments[0].homeScore() == 0
        arguments[0].awayScore() == 0
        arguments[0].startTime() instanceof LocalDateTime
        returnedMatch == Match.of(MEXICO, BRAZIL, arguments[0].startTime())
        0 * _
    }

    def "check calling appropriate methods for finishing match"() {
        given:
        def arguments = [] as Match[]

        when:
        scoreBoard.finishMatch(MATCH_A)

        then:
        1 * matchStore.removeMatch(_) >> {
            arguments = it
        }
        arguments[0] instanceof Match
        arguments[0] as Match == MATCH_A
        0 * _
    }

    def "check calling appropriate methods for updating score"() {
        given:
        def arguments = [] as Match[]
        def updatedMatch = MATCH_A.updateScore(1, 2)

        when:
        def returnedMatch = scoreBoard.updateScore(MATCH_A, 1, 2)

        then:
        1 * matchStore.updateMatch(_, _) >> {
            arguments = it
        }
        arguments[0] instanceof Match
        arguments[1] instanceof Match
        arguments[0] as Match == MATCH_A
        arguments[1] as Match == updatedMatch
        returnedMatch == updatedMatch
        0 * _
    }

    def "check calling appropriate methods for fetching matches"() {
        given:
        def expectedMatches = [MATCH_A, MATCH_B, MATCH_C] as Set

        when:
        def actualMatches = scoreBoard.getSummary()

        then:
        1 * matchStore.fetchOrderedMatches() >> expectedMatches
        actualMatches == expectedMatches
        0 * _
    }
}
