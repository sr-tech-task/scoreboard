package com.sportradar.model


import spock.lang.Specification

class TeamTest extends Specification {

    def "should throw IllegalArgumentException when code has inappropriate length"() {
        when:
        Team.of("Poland", "PL")
        then:
        def exception = thrown(IllegalArgumentException)
        exception.message == "Country code must be 3 characters long"
    }

    def "should successfully create Team object"() {
        when:
        def actualTeam = Team.of("Poland", "POL")
        then:
        actualTeam.country() == "Poland"
        actualTeam.code() == "POL"
    }
}
