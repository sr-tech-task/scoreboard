package com.sportradar.model


import spock.lang.Specification

import java.time.LocalDateTime

class MatchTest extends Specification {

    private static final Team POLAND = Team.of("Poland", "POL")
    private static final Team GERMANY = Team.of("Germany", "GER")
    private static final LocalDateTime MATCH_START = LocalDateTime.of(2021, 10, 1, 20, 45)

    def "should return copy of match with updated score"() {
        given:
        def match = Match.of(POLAND, GERMANY, MATCH_START)
        def expectedMatch = new Match(POLAND, GERMANY, 2, 1, MATCH_START)
        when:
        def actualMatch = match.updateScore(2,1)
        then:
        expectedMatch == actualMatch
        expectedMatch !== actualMatch
    }

    def "should throw IllegalArgumentException when score is negative"() {
        given:
        def match = Match.of(POLAND, GERMANY, MATCH_START)
        when:
        match.updateScore(-1,1)
        then:
        def exception = thrown(IllegalArgumentException)
        exception.message == "Score cannot be negative"
    }

    def "check getTotalScore method"() {
        given:
        def match = new Match(POLAND, GERMANY, 2, 1, MATCH_START)
        expect:
        match.getTotalScore() == 3
    }

    def "check toString method"() {
        given:
        def match = Match.of(POLAND, GERMANY, MATCH_START)
        when:
        def actualString = match.toString()
        then:
        true
        actualString == "Poland (POL) 0 - 0 Germany (GER)"
    }
}
