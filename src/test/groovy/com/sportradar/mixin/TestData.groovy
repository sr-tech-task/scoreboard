package com.sportradar.mixin

import com.sportradar.model.Match
import com.sportradar.model.Team

import java.time.LocalDateTime

trait TestData {
    static Team MEXICO = Team.of("Mexico", "MEX")
    static Team CANADA = Team.of("Canada", "CAN")
    static Team SPAIN = Team.of("Spain", "ESP")
    static Team BRAZIL = Team.of("Brazil", "BRA")
    static Team GERMANY = Team.of("Germany", "GER")
    static Team FRANCE = Team.of("France", "FRA")
    static Team URUGUAY = Team.of("Uruguay", "URU")
    static Team ITALY = Team.of("Italy", "ITA")
    static Team ARGENTINA = Team.of("Argentina", "ARG")
    static Team AUSTRALIA = Team.of("Australia", "AUS")

    static LocalDateTime MATCH_START_1 = LocalDateTime.of(2021, 10, 1, 20, 00)
    static LocalDateTime MATCH_START_2 = LocalDateTime.of(2021, 10, 1, 19, 45)

    static Match MATCH_A = new Match(MEXICO, CANADA, 0, 5, MATCH_START_1)
    static Match MATCH_B = new Match(SPAIN, BRAZIL, 10, 2, MATCH_START_1)
    static Match MATCH_C = new Match(GERMANY, FRANCE, 2, 2, MATCH_START_1)
    static Match MATCH_D = new Match(URUGUAY, ITALY, 6, 6, MATCH_START_2)
    static Match MATCH_E = new Match(ARGENTINA, AUSTRALIA, 3, 1, MATCH_START_2)
}