package com.sportradar.data

import com.sportradar.mixin.TestData
import spock.lang.Specification

class InMemoryMatchStoreTest extends Specification implements TestData {

    private MatchStore matchStore

    def setup() {
        matchStore = new InMemoryMatchStore()
    }

    def "should return empty set when no matches were added"() {
        when:
        def actualMatches = matchStore.fetchOrderedMatches()

        then:
        actualMatches.isEmpty()
    }

    def "should return set with one match when one match was added"() {
        when:
        def added = matchStore.addMatch(MATCH_A)
        def matches = matchStore.fetchOrderedMatches()

        then:
        added
        matches.size() == 1
        matches.contains(MATCH_A)

        when: "should not add same match again"
        added = matchStore.addMatch(MATCH_A)
        matches = matchStore.fetchOrderedMatches()

        then:
        !added
        matches.size() == 1
        matches.contains(MATCH_A)
    }

    def "should remove match from store"() {
        given:
        matchStore.addMatch(MATCH_A)
        matchStore.addMatch(MATCH_B)

        when:
        def removed = matchStore.removeMatch(MATCH_A)
        def matches = matchStore.fetchOrderedMatches()

        then:
        removed
        matches.size() == 1
        matches.contains(MATCH_B)
        !matches.contains(MATCH_A)

        when: "should not remove match that is not in store"
        removed = matchStore.removeMatch(MATCH_A)
        matches = matchStore.fetchOrderedMatches()

        then:
        !removed
        matches.size() == 1
        matches.contains(MATCH_B)
    }

    def "should store updated match in store"() {
        given:
        matchStore.addMatch(MATCH_A)
        def matchAUpdated = MATCH_A.updateScore(1, 5)
        when:
        def updated = matchStore.updateMatch(MATCH_A, matchAUpdated)
        def matches = matchStore.fetchOrderedMatches()

        then:
        updated
        matches.size() == 1
        matches.contains(matchAUpdated)
        !matches.contains(MATCH_A)

        when: "should not update match again"
        updated = matchStore.updateMatch(MATCH_A, matchAUpdated)
        matches = matchStore.fetchOrderedMatches()

        then:
        !updated
        matches.size() == 1
        matches.contains(matchAUpdated)
        !matches.contains(MATCH_A)
    }

    def "should return set of matches with appropriate sorting"() {
        when:
        matchStore.addMatch(MATCH_A)
        matchStore.addMatch(MATCH_B)
        matchStore.addMatch(MATCH_C)
        matchStore.addMatch(MATCH_D)
        matchStore.addMatch(MATCH_E)
        def actualMatches = matchStore.fetchOrderedMatches()

        then:
        actualMatches.size() == 5
        actualMatches[0] == MATCH_D
        actualMatches[1] == MATCH_B
        actualMatches[2] == MATCH_A
        actualMatches[3] == MATCH_E
        actualMatches[4] == MATCH_C
    }
}