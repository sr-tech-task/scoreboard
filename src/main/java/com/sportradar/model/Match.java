package com.sportradar.model;

import lombok.Builder;
import lombok.NonNull;

import java.time.LocalDateTime;

@Builder(toBuilder = true)
public record Match(@NonNull Team homeTeam,
                    @NonNull Team awayTeam,
                    int homeScore,
                    int awayScore,
                    @NonNull LocalDateTime startTime) {

    public static Match of(Team homeTeam, Team awayTeam, LocalDateTime startTime) {
        return new Match(homeTeam, awayTeam, 0, 0, startTime);
    }

    public Match updateScore(int homeScore, int awayScore) {
        if (homeScore < 0 || awayScore < 0) {
            throw new IllegalArgumentException("Score cannot be negative");
        }
        return this
                .toBuilder()
                .homeScore(homeScore)
                .awayScore(awayScore)
                .build();
    }

    public int getTotalScore() {
        return homeScore + awayScore;
    }

    @Override
    public String toString() {
        return "%s %d - %d %s".formatted(homeTeam, homeScore, awayScore, awayTeam);
    }
}