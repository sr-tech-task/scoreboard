package com.sportradar.model;

import java.util.Objects;

public record Team(String country,
                   String code) {

    public Team {
        Objects.requireNonNull(country, "Country cannot be null");
        Objects.requireNonNull(code, "Code cannot be null");
        if (code.length() != 3) throw new IllegalArgumentException("Country code must be 3 characters long");
    }

    public static Team of(String country, String abbreviation) {
        return new Team(country, abbreviation);
    }

    @Override
    public String toString() {
        return String.format("%s (%s)", country, code);
    }
}