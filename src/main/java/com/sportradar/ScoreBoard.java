package com.sportradar;

import com.sportradar.data.InMemoryMatchStore;
import com.sportradar.data.MatchStore;
import com.sportradar.model.Match;
import com.sportradar.model.Team;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.time.LocalDateTime;
import java.util.Set;

@Slf4j
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class ScoreBoard {

    private final MatchStore matchStore;

    public static ScoreBoard createWithStorage(MatchStore matchStore) {
        return new ScoreBoard(matchStore);
    }

    public static ScoreBoard createDefault() {
        return new ScoreBoard(new InMemoryMatchStore());
    }

    public Match startMatch(Team homeTeam, Team awayTeam) {

        log.debug(">>>[startMatch](homeTeam={},awayTeam={})", homeTeam, awayTeam);
        var match = Match.of(homeTeam, awayTeam, LocalDateTime.now());
        final var added = matchStore.addMatch(match);

        if (!added) {
            log.warn("Match already exists: {}", match);
            match = null;
        }

        log.debug("<<<[startMatch]:{}", match);
        return match;
    }

    public boolean finishMatch(Match match) {

        log.debug(">>>[finishMatch](match={})", match);
        final var removed = matchStore.removeMatch(match);
        if (!removed) {
            log.warn("Match not found: {}", match);
        }
        log.debug("<<<[finishMatch]:{}", removed);

        return removed;
    }

    public Match updateScore(Match match, int homeScore, int awayScore) {

        log.debug(">>>[updateScore](match={},homeScore={},awayScore={})", match, homeScore, awayScore);
        var updatedMatch = match.updateScore(homeScore, awayScore);
        final var updated = matchStore.updateMatch(match, updatedMatch);
        if (!updated) {
            log.warn("Match not found: {}", match);
        }
        log.debug("<<<[updateScore]:{}", updatedMatch);

        return updatedMatch;
    }

    public Set<Match> getSummary() {
        log.debug(">>>[getSummary]()");
        final var matches = matchStore.fetchOrderedMatches();
        log.debug("<<<[getSummary]:count({})", matches.size());
        return matches;
    }
}