package com.sportradar.data;

import com.sportradar.model.Match;
import com.sportradar.model.Team;
import lombok.NonNull;

import java.util.Comparator;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentSkipListSet;

public final class InMemoryMatchStore implements MatchStore {

    private final SortedSet<Match> matches;

    public InMemoryMatchStore() {
        matches = new ConcurrentSkipListSet<>(new MatchStoreComparator());
    }

    @Override
    public boolean addMatch(@NonNull Match match) {
        return matches.add(match);
    }

    @Override
    public boolean removeMatch(@NonNull Match match) {
        return matches.remove(match);
    }

    @Override
    public boolean updateMatch(Match match, Match updatedMatch) {
        if(matches.remove(match)) {
            matches.add(updatedMatch);
            return true;
        }
        return false;
    }

    @Override
    public Set<Match> fetchOrderedMatches() {
        return new TreeSet<>(matches);
    }

    private static class MatchStoreComparator implements Comparator<Match> {
        @Override
        public int compare(Match m1, Match m2) {
            return Comparator
                    .comparing(Match::getTotalScore)
                    .reversed()
                    .thenComparing(Match::startTime)
                    .thenComparing(Match::homeTeam, Comparator.comparing(Team::country))
                    .thenComparing(Match::awayTeam, Comparator.comparing(Team::country))
                    .compare(m1, m2);
        }
    }
}