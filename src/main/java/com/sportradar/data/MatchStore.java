package com.sportradar.data;


import com.sportradar.model.Match;

import java.util.Set;


public interface MatchStore {
    boolean addMatch(Match match);

    boolean removeMatch(Match match);

    boolean updateMatch(Match match, Match updatedMatch);

    Set<Match> fetchOrderedMatches();
}
