# Football World Cup Scoreboard Library

## Description
This is a library for a football world cup scoreboard management. It is used to keep track of the score of ongoing football matches. </br></br>
**The library provide the following functionalities:**
  - Add a match to the scoreboard
  - Update the score of an exact match
  - Remove a match from the scoreboard after it is finished
  - Get the current scoreboard state (the list of matches ordered by their total score in descending. If two matches have the same total score, the matches are ordered by their start time) 

## Usage
The main class of the library is the `ScoreBoard`. It is used to manage the scoreboard and supports all aforementioned operations </br>
The `ScoreBoard.createDefault()` factory method can be used to create a scoreboard with the default thread-safety in-memory implementation for data storage.</br>
The `ScoreBoard` also provides possibility to create an instance with a custom implementation for data storage. 
For this purpose the `ScoreBoard.createWithStorage(MatchStore matchStore)` factory method can be used. </br></br>
The `MatchStore` interface provides the following methods:
  - `boolean addMatch(Match match)` - adds a match to the storage
  - `boolean removeMatch(Match match)` - removes a match from the storage
  - `boolean updateMatch(Match match, Match updatedMatch)` - updates the score of an exact match
  - `Set<Match> fetchOrderedMatches()` - returns the current state of the storage.
If you decide to use a custom implementation for data storage, you should care about the thread-safety and appropriate sorting of the matches in the `fetchOrderedMatches()` method.</br></br>
The library operates with following immutable model classes:
  - `Match` - represents a football match
  - `Team` - represents a football team

## Example
```java
// Create a scoreboard with the default in-memory implementation for data storage
ScoreBoard scoreboard = ScoreBoard.createDefault();
// Start 1st match between two teams
final var mexicoTeam = Team.of("Mexico", "MEX");
final var canadaTeam = Team.of("Canada", "CAN");
var matchMexCan = scoreboard.startMatch(mexicoTeam, canadaTeam);
// Start 2nd match between two teams
final var spainTeam = Team.of("Spain", "ESP");
final var brazilTeam = Team.of("Brazil", "BRA");
var matchEspBra = scoreboard.startMatch(spainTeam, brazilTeam);
// Update score for 1st match
matchMexCan = scoreboard.updateScore(matchMexCan, 1, 0);
matchMexCan = scoreboard.updateScore(matchMexCan, 1, 1);
// Update score for 2nd match
matchEspBra = scoreboard.updateScore(matchEspBra, 0, 1);
// Get summary of ongoing matches
System.out.println("Summary of ongoing matches:");
scoreboard.getSummary().forEach(System.out::println);
// Finish 1st match
scoreboard.finishMatch(matchMexCan);
// Get summary of ongoing matches
System.out.println("Summary of ongoing matches:");
scoreboard.getSummary().forEach(System.out::println);
// Finish 2nd match
scoreboard.finishMatch(matchEspBra);
// Get summary of ongoing matches
System.out.println("Summary of ongoing matches:");
scoreboard.getSummary().forEach(System.out::println);

